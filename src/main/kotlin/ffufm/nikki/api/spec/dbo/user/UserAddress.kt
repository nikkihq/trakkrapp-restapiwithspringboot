package ffufm.nikki.api.spec.dbo.user

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.nikki.api.spec.dbo.user.UserUserSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * Address details of a user
 */
@Entity(name = "UserAddress")
@Table(name = "user_address")
data class UserAddress(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * street name of the address
     * Sample: Maceda St.
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "street"
    )
    val street: String = "",
    /**
     * Barangay name or number of the address
     * Sample: 506
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "barangay"
    )
    val barangay: String = "",
    /**
     * city name of the address
     * Sample: Manila
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "city"
    )
    val city: String = "",
    /**
     * province name of the address
     * Sample: NCR
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "province"
    )
    val province: String = "",
    /**
     * zipCode of the address
     * Sample: 1008
     */
    @Column(
        length = 5,
        updatable = true,
        nullable = false,
        name = "zip_code"
    )
    val zipCode: String = "",
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val user: UserUser? = null
) : PassDTOModel<UserAddress, UserAddressDTO, Long>() {
    override fun toDto(): UserAddressDTO = super.toDtoInternal(UserAddressSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<UserAddress, UserAddressDTO, Long>,
            UserAddressDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Address details of a user
 */
data class UserAddressDTO(
    val id: Long? = null,
    /**
     * street name of the address
     * Sample: Maceda St.
     */
    val street: String? = "",
    /**
     * Barangay name or number of the address
     * Sample: 506
     */
    val barangay: String? = "",
    /**
     * city name of the address
     * Sample: Manila
     */
    val city: String? = "",
    /**
     * province name of the address
     * Sample: NCR
     */
    val province: String? = "",
    /**
     * zipCode of the address
     * Sample: 1008
     */
    val zipCode: String? = "",
    val user: UserUserDTO? = null
) : PassDTO<UserAddress, Long>() {
    override fun toEntity(): UserAddress = super.toEntityInternal(UserAddressSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<UserAddress, PassDTO<UserAddress, Long>, Long>,
            PassDTO<UserAddress, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class UserAddressSerializer : PassDtoSerializer<UserAddress, UserAddressDTO, Long>() {
    override fun toDto(entity: UserAddress): UserAddressDTO = cycle(entity) {
        UserAddressDTO(
                id = entity.id,
        street = entity.street,
        barangay = entity.barangay,
        city = entity.city,
        province = entity.province,
        zipCode = entity.zipCode,
        user = entity.user?.idDto() ?: entity.user?.toDto()
                )}

    override fun toEntity(dto: UserAddressDTO): UserAddress = UserAddress(
            id = dto.id,
    street = dto.street ?: "",
    barangay = dto.barangay ?: "",
    city = dto.city ?: "",
    province = dto.province ?: "",
    zipCode = dto.zipCode ?: "",
    user = dto.user?.toEntity()
            )
    override fun idDto(id: Long): UserAddressDTO = UserAddressDTO(
            id = id,
    street = null,
    barangay = null,
    city = null,
    province = null,
    zipCode = null,

            )}

@Service("user.UserAddressValidator")
class UserAddressValidator : PassModelValidation<UserAddress> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<UserAddress>):
            ValidatorBuilder<UserAddress> = validatorBuilder.apply {
        konstraint(UserAddress::street) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserAddress::barangay) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserAddress::city) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserAddress::province) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserAddress::zipCode) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(5)
        }
        konstraintOnObject(UserAddress::user) {
            notNull()
        }
    }
}

@Service("user.UserAddressDTOValidator")
class UserAddressDTOValidator : PassModelValidation<UserAddressDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<UserAddressDTO>):
            ValidatorBuilder<UserAddressDTO> = validatorBuilder.apply {
        konstraint(UserAddressDTO::street) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserAddressDTO::barangay) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserAddressDTO::city) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserAddressDTO::province) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserAddressDTO::zipCode) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(5)
        }
        konstraintOnObject(UserAddressDTO::user) {
            notNull()
        }
    }
}
