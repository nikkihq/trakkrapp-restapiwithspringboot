package ffufm.nikki.api.spec.handler.user

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.nikki.api.spec.dbo.user.UserContactDetailDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface UserContactDetailDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully added Contact Detail
     */
    suspend fun createContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO

    /**
     * : 
     * HTTP Code 200: Successfully deleted the contact detail
     */
    suspend fun removeContactDetail(id: Long)

    /**
     * : 
     * HTTP Code 200: The updated model
     */
    suspend fun updateContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO
}

@Controller("user.ContactDetail")
class UserContactDetailHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: UserContactDetailDatabaseHandler

    /**
     * : 
     * HTTP Code 200: Successfully added Contact Detail
     */
    @RequestMapping(value = ["/users/{id:\\d+}/contact-details/"], method = [RequestMethod.POST])
    suspend fun createContactDetail(@RequestBody body: UserContactDetailDTO, @PathVariable("id")
            id: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.createContactDetail(body, id) }
    }

    /**
     * : 
     * HTTP Code 200: Successfully deleted the contact detail
     */
    @RequestMapping(value = ["/users/contact-details/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun removeContactDetail(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.removeContactDetail(id) }
    }

    /**
     * : 
     * HTTP Code 200: The updated model
     */
    @RequestMapping(value = ["/users/contact-details/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun updateContactDetail(@RequestBody body: UserContactDetailDTO, @PathVariable("id")
            id: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.updateContactDetail(body, id) }
    }
}
