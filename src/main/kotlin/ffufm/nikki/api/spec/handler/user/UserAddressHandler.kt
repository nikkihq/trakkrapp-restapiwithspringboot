package ffufm.nikki.api.spec.handler.user

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.nikki.api.spec.dbo.user.UserAddressDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface UserAddressDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully added the Address Details
     */
    suspend fun createdAddresses(body: UserAddressDTO, id: Long): UserAddressDTO

    /**
     * : 
     * HTTP Code 200: The updated Model
     */
    suspend fun updateAddresses(body: UserAddressDTO, id: Long): UserAddressDTO

    /**
     * : 
     * HTTP Code 200: Successfully deleted the Addresses
     */
    suspend fun removeAddresses(id: Long)
}

@Controller("user.Address")
class UserAddressHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: UserAddressDatabaseHandler

    /**
     * : 
     * HTTP Code 200: Successfully added the Address Details
     */
    @RequestMapping(value = ["/users/{id:\\d+}/addresses/"], method = [RequestMethod.POST])
    suspend fun createdAddresses(@RequestBody body: UserAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.createdAddresses(body, id) }
    }

    /**
     * : 
     * HTTP Code 200: The updated Model
     */
    @RequestMapping(value = ["/users/addresses/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun updateAddresses(@RequestBody body: UserAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.updateAddresses(body, id) }
    }

    /**
     * : 
     * HTTP Code 200: Successfully deleted the Addresses
     */
    @RequestMapping(value = ["/users/addresses/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun removeAddresses(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.removeAddresses(id) }
    }
}
