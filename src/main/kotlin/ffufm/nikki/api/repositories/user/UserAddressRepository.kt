package ffufm.nikki.api.repositories.user

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.nikki.api.spec.dbo.user.UserAddress
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UserAddressRepository : PassRepository<UserAddress, Long> {
//    @Query(
//        "SELECT t from UserAddress t LEFT JOIN FETCH t.user",
//        countQuery = "SELECT count(id) FROM UserAddress"
//    )
//    fun findAllAndFetchUser(pageable: Pageable): Page<UserAddress>
//
//
    @Query(
        """
            SELECT CASE WHEN COUNT(a) > 0 THEN TRUE ELSE FALSE END
            FROM UserAddress a WHERE a.street = :street
        """
    )
    fun doesStreetExist(street: String): Boolean

    @Query(
        """
            SELECT CASE WHEN COUNT(a) > 0 THEN TRUE ELSE FALSE END
            FROM UserAddress a WHERE a.barangay = :barangay
        """
    )
    fun doesBarangayExist(barangay: String): Boolean


    @Query(
        """
            SELECT CASE WHEN COUNT(a) > 0 THEN TRUE ELSE FALSE END
            FROM UserAddress a WHERE a.city = :city
        """
    )
    fun doesCityExist(city: String): Boolean

    @Query(
        """
            SELECT CASE WHEN COUNT(a) > 0 THEN TRUE ELSE FALSE END
            FROM UserAddress a WHERE a.province = :province
        """
    )

    fun doesProvinceExist(province: String): Boolean

    @Query(
        """
            SELECT CASE WHEN COUNT(a) > 0 THEN TRUE ELSE FALSE END
            FROM UserAddress a WHERE a.zipCode = :zipCode
        """
    )
    fun doesZipCodeExist(zipCode: String): Boolean

}
