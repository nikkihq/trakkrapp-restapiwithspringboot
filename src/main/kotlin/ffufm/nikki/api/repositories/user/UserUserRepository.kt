package ffufm.nikki.api.repositories.user

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.nikki.api.spec.dbo.user.UserUser
import org.springframework.data.jpa.repository.Query
import kotlin.Long
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserUserRepository : PassRepository<UserUser, Long>{

    @Query(
        """
            SELECT CASE WHEN COUNT(u) > 0 THEN TRUE ELSE FALSE END
            FROM UserUser u WHERE u.email = :email
        """
    )
    fun doesEmailExist(email: String): Boolean

    @Query(
        """
            SELECT u FROM UserUser u
            LEFT JOIN FETCH u.contactDetails cd
            WHERE u.id = :id
        """
    )

    fun getUserWithContactDetails(id: Long): Optional<UserUser>
}
