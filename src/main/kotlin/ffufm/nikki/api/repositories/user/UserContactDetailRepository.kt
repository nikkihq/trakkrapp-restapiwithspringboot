package ffufm.nikki.api.repositories.user

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.nikki.api.spec.dbo.user.UserContactDetail
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface UserContactDetailRepository : PassRepository<UserContactDetail, Long> {
//    @Query(
//        "SELECT t from UserContactDetail t LEFT JOIN FETCH t.user",
//        countQuery = "SELECT count(id) FROM UserContactDetail"
//    )
//    fun findAllAndFetchUser(pageable: Pageable): Page<UserContactDetail>

    @Query(
        """
            SELECT COUNT(cd) FROM UserContactDetail cd WHERE cd.user.id = :id
        """
    )
    fun countContactDetail(id: Long): Int

    @Query(
        """
            SELECT CASE WHEN COUNT(c) > 0 THEN TRUE ELSE FALSE END
            FROM UserContactDetail c WHERE c.contactDetails = :contactDetails
        """
    )fun doesContactDetailsExist(contactDetails: String): Boolean

    @Modifying
    @Transactional
    @Query(
        """
            UPDATE UserContactDetail c SET c.isPrimary = false
            WHERE c.user.id = :id
        """
    )
    fun updatedIsPrimaryToFalse(id: Long): Unit
}
