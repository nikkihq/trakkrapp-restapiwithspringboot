package ffufm.nikki.api.handlerimpl.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.nikki.api.repositories.user.UserUserRepository
import ffufm.nikki.api.spec.dbo.user.UserUser
import ffufm.nikki.api.spec.dbo.user.UserUserDTO
import ffufm.nikki.api.spec.handler.user.UserUserDatabaseHandler
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("user.UserUserHandler")
class UserUserHandlerImpl : PassDatabaseHandler<UserUser, UserUserRepository>(),
        UserUserDatabaseHandler {
    /**
     * Get all Users by page: Returns all Users from the system that the user has access to. The
     * Headers will include TotalElements, TotalPages, CurrentPage and PerPage to help with Pagination.
     * HTTP Code 200: List of Users
     */
    override suspend fun getAll(maxResults: Int, page: Int): Page<UserUserDTO> {
        val pagination = PageRequest.of(maxResults, page)

        return repository.findAll(pagination).toDtos()
    }

    /**
     * Finds Users by ID: Returns Users based on ID
     * HTTP Code 200: The User object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): UserUserDTO? {

        return repository.getUserWithContactDetails(id).orElseThrow404(id).toDto()
    }

    /**
     * Create User: Creates a new User object
     * HTTP Code 201: The created User
     */
    override suspend fun create(body: UserUserDTO): UserUserDTO {
        val bodyEntity = body.toEntity()

        if(repository.doesEmailExist(bodyEntity.email)){
            throw ResponseStatusException(HttpStatus.CONFLICT,
            "Email ${bodyEntity.email} already exist")
        }

        val userType = UserUser.Usertype.valueOf(bodyEntity.userType.uppercase())

        return repository.save(bodyEntity.copy(
            userType = userType.value
        )).toDto()
    }

    /**
     * Update the User: Updates an existing User
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: UserUserDTO, id: Long): UserUserDTO {
        val original = repository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()

        return repository.save(original.copy(
            email = bodyEntity.email,
            firstName = bodyEntity.firstName,
            lastName = bodyEntity.lastName
        )).toDto()
    }

    /**
     * Delete User by id.: Deletes one specific User.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }
}
