package ffufm.nikki.api.handlerimpl.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.nikki.api.repositories.user.UserContactDetailRepository
import ffufm.nikki.api.repositories.user.UserUserRepository
import ffufm.nikki.api.spec.dbo.user.UserContactDetail
import ffufm.nikki.api.spec.dbo.user.UserContactDetailDTO
import ffufm.nikki.api.spec.dbo.user.UserUser
import ffufm.nikki.api.spec.handler.user.UserContactDetailDatabaseHandler
import ffufm.nikki.api.utils.Constants
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException
import javax.transaction.Transactional

@Component("user.UserContactDetailHandler")
class UserContactDetailHandlerImpl(
    private val userRepository: UserUserRepository
) : PassDatabaseHandler<UserContactDetail,
        UserContactDetailRepository>(), UserContactDetailDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully added Contact Detail
     */
    @Transactional
    override suspend fun createContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO {
        val user = userRepository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()

        val contactDetailCount = repository.countContactDetail(id)
        if(contactDetailCount >= Constants.MAX_CONTACT_DETAILS){
            throw ResponseStatusException(HttpStatus.BAD_REQUEST,
                "You cannot create more than four contact details")
        }

        if(repository.doesContactDetailsExist(bodyEntity.contactDetails)){
            throw ResponseStatusException(HttpStatus.CONFLICT,
                "Contact Details with details: ${bodyEntity.contactDetails} already exists!")
        }

        if(bodyEntity.isPrimary){
            repository.updatedIsPrimaryToFalse(id)
        }

        return repository.save(bodyEntity.copy(
            user = user
        )).toDto()
    }

    /**
     * : 
     * HTTP Code 200: Successfully deleted the contact detail
     */
    override suspend fun removeContactDetail(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * : 
     * HTTP Code 200: The updated model
     */
    override suspend fun updateContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.save(original.copy(
            contactDetails = body.contactDetails!!,
            contactType = body.contactType!!
        )).toDto()
    }
}
