package ffufm.nikki.api.handlerimpl.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.nikki.api.repositories.user.UserAddressRepository
import ffufm.nikki.api.repositories.user.UserUserRepository
import ffufm.nikki.api.spec.dbo.user.UserAddress
import ffufm.nikki.api.spec.dbo.user.UserAddressDTO
import ffufm.nikki.api.spec.handler.user.UserAddressDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("user.UserAddressHandler")
class UserAddressHandlerImpl(
    private val userRepository: UserUserRepository
) : PassDatabaseHandler<UserAddress, UserAddressRepository>(),
        UserAddressDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully added the Address Details
     */
    override suspend fun createdAddresses(body: UserAddressDTO, id: Long): UserAddressDTO {
        val user = userRepository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()

        if(repository.doesStreetExist(bodyEntity.street) && repository.doesBarangayExist(bodyEntity.barangay) &&
            repository.doesCityExist(bodyEntity.city) && repository.doesProvinceExist(bodyEntity.province) &&
            repository.doesZipCodeExist(bodyEntity.zipCode)){
            throw ResponseStatusException(HttpStatus.CONFLICT,
                "Addresses with id: $id already exists!")
        }

        return repository.save(bodyEntity.copy(
            user = user
        )).toDto()
    }

    /**
     * : 
     * HTTP Code 200: The updated Model
     */
    override suspend fun updateAddresses(body: UserAddressDTO, id: Long): UserAddressDTO {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.save(original.copy(
            street = body.street!!,
            barangay = body.barangay!!,
            city = body.city!!,
            province = body.province!!,
            zipCode = body.zipCode!!
        )).toDto()
    }

    /**
     * : 
     * HTTP Code 200: Successfully deleted the Addresses
     */
    override suspend fun removeAddresses(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.delete(original)
    }
}
