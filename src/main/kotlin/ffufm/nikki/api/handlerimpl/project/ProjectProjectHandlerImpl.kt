package ffufm.nikki.api.handlerimpl.project

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.nikki.api.repositories.project.ProjectProjectRepository
import ffufm.nikki.api.spec.dbo.project.ProjectProject
import ffufm.nikki.api.spec.dbo.project.ProjectProjectDTO
import ffufm.nikki.api.spec.handler.project.ProjectProjectDatabaseHandler
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component

@Component("project.ProjectProjectHandler")
class ProjectProjectHandlerImpl: PassDatabaseHandler<ProjectProject, ProjectProjectRepository>
    (), ProjectProjectDatabaseHandler {

    override suspend fun create(body: ProjectProjectDTO): ProjectProjectDTO {
        return repository.save(body.toEntity()).toDto()
    }

    override suspend fun getAll(maxResults: Int, page: Int): Page<ProjectProjectDTO> {
        val pagination = PageRequest.of(maxResults, page)

        return repository.findAll(pagination).toDtos()
    }

    override suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {
        val original = repository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()

        return repository.save(original.copy(
            name = bodyEntity.name,
            description = bodyEntity.description,
            status = bodyEntity.status
        )).toDto()
    }
}