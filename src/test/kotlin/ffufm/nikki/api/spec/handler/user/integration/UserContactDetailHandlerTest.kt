package ffufm.nikki.api.spec.handler.user.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.nikki.api.PassTestBase
import ffufm.nikki.api.repositories.user.UserContactDetailRepository
import ffufm.nikki.api.spec.dbo.user.UserContactDetail
import ffufm.nikki.api.spec.handler.user.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class UserContactDetailHandlerTest : PassTestBase() {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc


    @Test
    @WithMockUser
    fun `createContactDetail should return 200`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val contactDetail = EntityGenerator.createContactDetails().copy(user =user)
        val body = userContactDetailRepository.save(contactDetail)

                mockMvc.post("/users/{id}/contact-details/", user.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }
                }
    }

    @Test
    @WithMockUser
    fun `createContactDetail should return 400 on the 4th entry`(){
        val user = userUserRepository.save(EntityGenerator.createUser())
        val contactDetail = EntityGenerator.createContactDetails().copy(user =user)

        val createdContactList = userContactDetailRepository.saveAll(
            listOf(
                contactDetail,
                contactDetail.copy(contactDetails = "09495867857", contactType = "mobile"),
                contactDetail.copy(contactDetails = "09586754748", contactType = "mobile"),
                contactDetail.copy(contactDetails = "09586744748", contactType = "mobile")

            )
        )
        mockMvc.post("/users/{id}/contact-details/", user.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(createdContactList)
        }.andExpect {
            status { isBadRequest() }
        }
    }

    @Test
    @WithMockUser
    fun `create contactDetails should return 409 given duplicate contactDetails`(){
        val user = userUserRepository.save(EntityGenerator.createUser())
        val contact = EntityGenerator.createContactDetails().copy(user = user)
        val createContact = userContactDetailRepository.save(contact)

        val duplicateContact = createContact.copy(
            contactType = "phone"
        )

        mockMvc.post("/users/{id}/contact-details/", user.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(duplicateContact)
        }.andExpect {
            status { isConflict() }
        }
    }

    @Test
    @WithMockUser
    fun `removeContactDetail should delete contactDetail`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val contactDetail = EntityGenerator.createContactDetails().copy(user =user)
        val createContact = userContactDetailRepository.save(contactDetail)
                mockMvc.delete("/users/contact-details/{id}/", user.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON

                }.andExpect {
                    status { isOk() }

                }
    }


    @Test
    @WithMockUser
    fun `test updateContactDetail`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val contactDetail = EntityGenerator.createContactDetails().copy(user = user)
        val body = userContactDetailRepository.save(contactDetail)

        val updateBody = body.copy(
            contactDetails = "09945867432",
        )
                mockMvc.put("/users/contact-details/{id}/", user.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(updateBody)
                }.asyncDispatch().andExpect {
                    status { isOk() }

                }
    }
}
