package ffufm.nikki.api.spec.handler.project.implementation

import ffufm.nikki.api.PassTestBase
import ffufm.nikki.api.repositories.project.ProjectProjectRepository
import ffufm.nikki.api.spec.handler.project.ProjectProjectDatabaseHandler
import ffufm.nikki.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class ProjectProjectDatabaseHandlerTest: PassTestBase() {

    @Autowired
    private lateinit var projectProjectDatabaseHandler: ProjectProjectDatabaseHandler


    @Test
    fun `getAll projects`() = runBlocking {
        val body = EntityGenerator.createProject()
        projectProjectRepository.saveAll(
            listOf(
                body,
                body.copy(name = "update task", description = "updating the task", status = "pending")
            )
        )
        val maxResults: Int = 100
        val page: Int = 1
        projectProjectDatabaseHandler.getAll(maxResults, page)
        assertEquals(1, projectProjectDatabaseHandler.getAll(maxResults, page).size)
    }

    @Test
    fun `createProject should return created project`() = runBlocking {
        assertEquals(0, projectProjectRepository.findAll().size)

        val project = EntityGenerator.createProject().toDto()
        projectProjectDatabaseHandler.create(project)

        assertEquals(1, projectProjectRepository.findAll().size)
    }

    @Test
    fun `updateProject should return updated project given valid id and input`() = runBlocking{
        val originalProject = projectProjectRepository.save(EntityGenerator.createProject())
        val body = originalProject.copy(
            name = "update project",
            description = "should be able to update a project",
            status = "pending"
        )

        val updatedProject = projectProjectDatabaseHandler.update(body.toDto(), originalProject.id!!)
        assertEquals(body.name, updatedProject.name)
        assertEquals(body.description, updatedProject.description)
        assertEquals(body.status, updatedProject.status)
    }
}