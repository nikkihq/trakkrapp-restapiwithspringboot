package ffufm.nikki.api.spec.handler.user.utils

import ffufm.nikki.api.spec.dbo.project.ProjectProject
import ffufm.nikki.api.spec.dbo.user.UserAddress
import ffufm.nikki.api.spec.dbo.user.UserContactDetail
import ffufm.nikki.api.spec.dbo.user.UserUser

object EntityGenerator {

    fun createUser(): UserUser = UserUser(
        email = "brandon@brandon.com",
        firstName = "Brandon",
        lastName = "Cruz",
        userType = "GC"
    )

    fun createContactDetails(): UserContactDetail = UserContactDetail(
        contactDetails = "09494536869",
        contactType = "Phone"
    )

    fun createAddresses(): UserAddress = UserAddress(
        street = "Maceda",
        barangay = "506",
        city = "Manila",
        province = "NCR",
        zipCode = "1008"
    )

    fun createProject(): ProjectProject = ProjectProject(
        name = "create project",
        description = "should be able to create a project",
        status = "pending"
    )


}