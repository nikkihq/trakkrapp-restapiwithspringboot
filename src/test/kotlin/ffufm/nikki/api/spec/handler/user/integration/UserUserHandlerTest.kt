package ffufm.nikki.api.spec.handler.user.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.nikki.api.PassTestBase
import ffufm.nikki.api.repositories.user.UserUserRepository
import ffufm.nikki.api.spec.dbo.user.UserUser
import ffufm.nikki.api.spec.handler.user.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put


class UserUserHandlerTest : PassTestBase() {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc


    @Test
    @WithMockUser
    fun `test getAll`() {
                mockMvc.get("/users/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON

                }.andExpect {
                    status { isOk() }

                }
    }

    @Test
    @WithMockUser
    fun `getById should return isOk`() {
        val body = EntityGenerator.createUser()
        val user = userUserRepository.save(body)

        mockMvc.get("/users/{id}/", user.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `getById should return 404 given invalid user id`(){
        val invalidId: Long = 123
        mockMvc.get("/users/{id}/", invalidId) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isNotFound() }
        }

    }

    @Test
    @WithMockUser
    fun `createUser should return 200`() {
        val body = EntityGenerator.createUser()
                mockMvc.post("/users/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                }
    }


//    @Test
//    @WithMockUser
//    fun `createUser should return 409 given duplicate email`(){
//        val user = userUserRepository.save(EntityGenerator.createUser())
//        val duplicateEmail = user.copy(
//            firstName = "Kate"
//        )
//        mockMvc.post("/users/") {
//            accept(MediaType.APPLICATION_JSON)
//            contentType = MediaType.APPLICATION_JSON
//            content = objectMapper.writeValueAsString(duplicateEmail)
//        }.andExpect {
//            status { isConflict() }
//        }
//
//    }


    @Test
    @WithMockUser
    fun `updateUser should create user details`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val body = user.copy(
            firstName = "brends",
            lastName = "brenny",
            email = "brenny@bren.com"
            )

                mockMvc.put("/users/{id}/", user.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }

                }
    }

    @Test
    @WithMockUser
    fun `removeUser should delete userDetails`() {
        val user = userUserRepository.save(EntityGenerator.createUser())

                mockMvc.delete("/users/{id}/", user.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON

                }.andExpect {
                    status { isOk() }

                }
    }
}
