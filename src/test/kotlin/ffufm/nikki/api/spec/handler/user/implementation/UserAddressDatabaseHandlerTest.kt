package ffufm.nikki.api.spec.handler.user.implementation

import ffufm.nikki.api.PassTestBase
import ffufm.nikki.api.repositories.user.UserAddressRepository
import ffufm.nikki.api.spec.dbo.user.UserAddress
import ffufm.nikki.api.spec.handler.user.UserAddressDatabaseHandler
import ffufm.nikki.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class UserAddressDatabaseHandlerTest : PassTestBase() {

    @Autowired
    lateinit var userAddressDatabaseHandler: UserAddressDatabaseHandler


    @Test
    fun `test createdAddresses`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())

        assertEquals(0, userAddressRepository.findAll().size)
        val address = EntityGenerator.createAddresses()
        val createAddress = userAddressDatabaseHandler.createdAddresses(address.toDto(), user.id!!)
        assertEquals(1, userAddressRepository.findAll().size)
        assertEquals(address.street, createAddress.street)
        assertEquals(address.barangay, createAddress.barangay)
        assertEquals(address.city, createAddress.city)
        assertEquals(address.province, createAddress.province)
        assertEquals(address.zipCode, createAddress.zipCode)
    }

    @Test
    fun `createAddress should fail given all duplicate address details`() = runBlocking{
        val user = userUserRepository.save(EntityGenerator.createUser())

        val addressDetail = EntityGenerator.createAddresses().copy(user = user)
        val createAddress = userAddressRepository.save(addressDetail)

        val duplicateAddress = createAddress.copy(
            street = "Maceda",
            barangay = "506",
            city = "Manila",
            province = "NCR",
            zipCode = "1008"
        )
        val exception = assertFailsWith<ResponseStatusException> {
            userAddressDatabaseHandler.createdAddresses(duplicateAddress.toDto(), user.id!!)
        }
        val expectedException =
            "409 CONFLICT \"Addresses with id: ${duplicateAddress.id} already exists!\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `test updateAddresses`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val address = EntityGenerator.createAddresses().copy(user = user)
        val createAddress = userAddressRepository.save(address)

        val body = createAddress.copy(
            street = "pymargal"
        )

        val updateAddress = userAddressDatabaseHandler.updateAddresses(body.toDto(), user.id!!)
        assertEquals(body.street, updateAddress.street)
    }

    @Test
    fun `test removeAddresses`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val address = EntityGenerator.createAddresses().copy(user = user)
        val createAddress = userAddressRepository.save(address)

        assertEquals(1, userAddressRepository.findAll().size)
        userAddressDatabaseHandler.removeAddresses(user.id!!)
        assertEquals(0, userAddressRepository.findAll().size)
    }
}
