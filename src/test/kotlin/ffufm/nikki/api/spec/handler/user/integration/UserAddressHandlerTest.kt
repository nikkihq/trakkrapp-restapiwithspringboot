package ffufm.nikki.api.spec.handler.user.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.nikki.api.PassTestBase
import ffufm.nikki.api.repositories.user.UserAddressRepository
import ffufm.nikki.api.spec.dbo.user.UserAddress
import ffufm.nikki.api.spec.handler.user.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class UserAddressHandlerTest : PassTestBase() {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc


    @Test
    @WithMockUser
    fun `test createdAddresses`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val address = EntityGenerator.createAddresses().copy(user = user)
        val body = userAddressRepository.save(address)

                mockMvc.post("/users/{id}/addresses/", user.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.asyncDispatch().andExpect {
                    status { isOk() }

                }
    }


    @Test
    @WithMockUser
    fun `test updateAddresses`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val address = EntityGenerator.createAddresses().copy(user = user)
        val body = userAddressRepository.save(address)

        val updateBody = body.copy(
            street = "espana"
        )

                mockMvc.put("/users/addresses/{id}/", user.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(updateBody)
                }.asyncDispatch().andExpect {
                    status { isOk() }

                }
    }

    @Test
    @WithMockUser
    fun `test removeAddresses`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val address = EntityGenerator.createAddresses().copy(user = user)
        userAddressRepository.save(address)

                mockMvc.delete("/users/addresses/{id}/", user.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON

                }.asyncDispatch().andExpect {
                    status { isOk() }

                }
    }
}
