package ffufm.nikki.api.spec.handler.user.implementation

import ffufm.nikki.api.PassTestBase
import ffufm.nikki.api.repositories.user.UserContactDetailRepository
import ffufm.nikki.api.spec.dbo.user.UserContactDetail
import ffufm.nikki.api.spec.handler.user.UserContactDetailDatabaseHandler
import ffufm.nikki.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class UserContactDetailDatabaseHandlerTest : PassTestBase() {

    @Autowired
    lateinit var userContactDetailDatabaseHandler: UserContactDetailDatabaseHandler


    @Test
    fun `createContactDetail should create contact details`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())
        assertEquals(0, userContactDetailRepository.findAll().size)

        val contactDetail = EntityGenerator.createContactDetails()
        val createContactDetails = userContactDetailDatabaseHandler.createContactDetail(contactDetail.toDto(), user.id!!)
        assertEquals(1, userContactDetailRepository.findAll().size)
        assertEquals(contactDetail.contactDetails, createContactDetails.contactDetails)
        assertEquals(contactDetail.contactType, createContactDetails.contactType)
    }

    @Test
    fun `createContactDetails should throw an error given maximum record is reached`() = runBlocking{
        val user = userUserRepository.save(EntityGenerator.createUser())
        val body = EntityGenerator.createContactDetails().copy(user = user)

        val contactDetail = userContactDetailRepository.saveAll(
            listOf(
                body,
                body.copy(contactDetails = "09494536867", contactType = "phone"),
                body.copy(contactDetails = "09494536863", contactType = "phone")

            )
        )
        val exception = assertFailsWith<ResponseStatusException> {
            userContactDetailDatabaseHandler.createContactDetail(body.copy(
                contactDetails = "095494567589",
                contactType = "mobile"
            ).toDto(), user.id!!)
        }

        val expectedException = "400 BAD_REQUEST \"You cannot create more than four contact details\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `createContactDetail should throw an error given duplicate contactDetail`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val contactDetail = EntityGenerator.createContactDetails().copy(user = user)
        val createContact = userContactDetailRepository.save(contactDetail)

        val duplicateContactDetail = createContact.copy(
            contactDetails = "09494536869",
            contactType = "mobile"
        )

        val exception = assertFailsWith<ResponseStatusException> {
            userContactDetailDatabaseHandler.createContactDetail(duplicateContactDetail.toDto()
                , user.id!!)
        }
        val expectedException =
            "409 CONFLICT \"Contact Details with details: ${duplicateContactDetail.contactDetails} already exists!\""
        assertEquals(expectedException, exception.message)
    }


    @Test
    fun `removeContactDetail should remove contact`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())

        val contactDetail = EntityGenerator.createContactDetails().copy(user = user)
        val createdContact = userContactDetailRepository.save(contactDetail)

        assertEquals(1, userContactDetailRepository.findAll().size)
        userContactDetailDatabaseHandler.removeContactDetail(createdContact.id!!)
        assertEquals(0, userContactDetailRepository.findAll().size)
    }

    @Test
    fun `updateContactDetail should return updated contactdetail`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())

        val contactDetails = EntityGenerator.createContactDetails().copy(user = user)
        val createContact = userContactDetailRepository.save(contactDetails)
        val body = createContact.copy(
            contactDetails = "094945647593",
            contactType = "mobile"
        )
        val updatedContact = userContactDetailDatabaseHandler.updateContactDetail(body.toDto(), createContact.id!!)
        assertEquals(body.contactDetails, updatedContact.contactDetails)
        assertEquals(body.contactType, updatedContact.contactType)

    }
}
