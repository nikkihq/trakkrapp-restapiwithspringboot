package ffufm.nikki.api.spec.handler.user.implementation

import ffufm.nikki.api.PassTestBase
import ffufm.nikki.api.repositories.user.UserUserRepository
import ffufm.nikki.api.spec.dbo.user.UserUser
import ffufm.nikki.api.spec.dbo.user.UserUserDTO
import ffufm.nikki.api.spec.handler.user.UserUserDatabaseHandler
import ffufm.nikki.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class UserUserDatabaseHandlerTest : PassTestBase() {

    @Autowired
    lateinit var userUserDatabaseHandler: UserUserDatabaseHandler


    @Test
    fun `getAll users`() = runBlocking {
        val body = EntityGenerator.createUser()
        userUserRepository.saveAll(
            listOf(
                body,
                body.copy(email = "bren@bren.com")
            )
        )
        val maxResults: Int = 100
        val page: Int = 1
        userUserDatabaseHandler.getAll(maxResults, page)
        assertEquals(1, userUserDatabaseHandler.getAll(maxResults, page).size)
    }

    @Test
    fun `getById should return a user`() = runBlocking {
        val user = EntityGenerator.createUser()
        val savedUser = userUserRepository.save(user)
        userUserDatabaseHandler.getById(savedUser.id!!)
        assertEquals(1, userUserRepository.findAll().size)
    }

    @Test
    fun `getById should return an error given invalid Id`() = runBlocking{
        val user = userUserRepository.save(EntityGenerator.createUser())
        val invalidId: Long = 123

        val exception = assertFailsWith<ResponseStatusException> {
            userUserDatabaseHandler.getById(invalidId)
        }
        val expectedException = "404 NOT_FOUND \"UserUser with ID $invalidId not found\""
        assertEquals(expectedException, exception.message)
    }


    @Test
    fun `createUser should return created user`() = runBlocking {
        assertEquals(0, userUserRepository.findAll().size)
        val user = EntityGenerator.createUser().toDto()
        userUserDatabaseHandler.create(user)
        assertEquals(1, userUserRepository.findAll().size)
    }

    @Test
    fun `createUser should return an error given duplicate email`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val duplicateUser = user.copy(
            firstName = "kate",
            lastName = "Sta Ana"
        )

        val exception = assertFailsWith<ResponseStatusException> {
            userUserDatabaseHandler.create(duplicateUser.toDto())
        }
        val expectedException = "409 CONFLICT \"Email ${duplicateUser.email} already exist\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `updateUser should return updated user`() = runBlocking {
        val originalUser = EntityGenerator.createUser()
        val createOriginalUser = userUserRepository.save(originalUser)

        val body = createOriginalUser.copy(
            email = "brenda@brenda.com",
            firstName = "Brenda",
            lastName = "Mage"
        )
        val updatedUser = userUserDatabaseHandler.update(body.toDto(), createOriginalUser.id!!)
        assertEquals(body.firstName, updatedUser.firstName)
        assertEquals(body.lastName, updatedUser.lastName)
        assertEquals(body.email, updatedUser.email)
    }

    @Test
    fun `updateUser should return an error given invalid id`() = runBlocking{
        val user = userUserRepository.save(EntityGenerator.createUser())
        val invalidId: Long = 123

        val updatedBody = user.copy(
            email = "brenda@brenda.com",
            firstName = "Brenda",
            lastName = "Mage"
        )

        val exception = assertFailsWith<ResponseStatusException> {
            userUserDatabaseHandler.update(updatedBody.toDto(), invalidId)
        }
        val expectedException = "404 NOT_FOUND \"UserUser with ID $invalidId not found\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `test remove`() = runBlocking {
        val user = EntityGenerator.createUser()
        val createdUser = userUserRepository.save(user)

        assertEquals(1, userUserRepository.findAll().size)
        userUserDatabaseHandler.remove(createdUser.id!!)
        assertEquals(0, userUserRepository.findAll().size)

    }

    @Test
    fun `remove user will return an error given invalid id`() = runBlocking{
        val user = userUserRepository.save(EntityGenerator.createUser())
        val invalidId: Long = 123

        val exception = assertFailsWith<ResponseStatusException> {
            userUserDatabaseHandler.remove(invalidId)
        }
        val expectedException = "404 NOT_FOUND \"UserUser with ID $invalidId not found\""
        assertEquals(expectedException, exception.message)
    }
}
