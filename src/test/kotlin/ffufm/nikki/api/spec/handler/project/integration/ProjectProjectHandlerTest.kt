package ffufm.nikki.api.spec.handler.project.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.nikki.api.PassTestBase
import ffufm.nikki.api.spec.handler.user.utils.EntityGenerator
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class ProjectProjectHandlerTest: PassTestBase() {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc


    @Test
    @WithMockUser
    fun `getAll projects should return 200`() {
        mockMvc.get("/projects/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON

        }.andExpect {
            status { isOk() }

        }
    }


    @Test
    @WithMockUser
    fun `createProject should return 200`(){
        val project = EntityGenerator.createProject()

        mockMvc.post("/projects/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(project)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `updateProject should return 200`(){
        val project = projectProjectRepository.save(EntityGenerator.createProject())

        val updateProject = project.copy(
            name = "update project",
            description = "should be able to update a project",
            status = "pending"
        )

        mockMvc.put("/projects/{id:\\d+}/", project.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(updateProject)
        }.andExpect {
            status { isOk() }

        }
    }
}